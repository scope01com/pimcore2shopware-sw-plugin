/**
 * initialize search result tabs
 */
$('.tab-menu--search').swTabMenu();

// change tab if content paging
$('.tab--link.has--content.search').click();
