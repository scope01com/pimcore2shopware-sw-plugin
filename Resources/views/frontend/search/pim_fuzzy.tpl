{extends file='parent:frontend/search/fuzzy.tpl'}
{namespace name="frontend/search/fuzzy"}

{block name='frontend_index_content'}
    <div class="content search--content">

        {block name='frontend_search_info_messages'}
            {if !$sSearchResults.sArticles && $searchResultsContent.count eq 0}
                {if $sRequests.sSearchOrginal}

                    {* No results found *}
                    {block name='frontend_search_message_no_results'}
                        {include file="frontend/_includes/messages.tpl" type="warning" content="{s name='SearchFuzzyHeadlineNoResult'}{/s}"}
                    {/block}
                {else}

                    {* Given search term is too short *}
                    {block name='frontend_search_message_shortterm'}
                        {include file="frontend/_includes/messages.tpl" type="error" content="{s name='SearchFuzzyInfoShortTerm'}{/s}"}
                    {/block}
                {/if}
            {/if}
        {/block}
        {if $sSearchResults.sArticles || $searchResultsContent gt 0}

            {* Listing varibles *}
            {block name="frontend_search_variables"}
                {$sArticles = $sSearchResults.sArticles}
                {$sNumberArticles = $sSearchResults.sArticlesCount}
                {$sTemplate = "listing"}
                {$sBoxMode = "table"}
                {$showListing = true}
                {$pages = ceil($sNumberArticles / $criteria->getLimit())}
                {$countCtrlUrl = "{url module="widgets" controller="listing" action="listingCount" params=$ajaxCountUrlParams fullPath}"}
            {/block}

            {block name='frontend_search_headline'}
                <h1 class="search--headline">
                    {s name=pim_search_to}Zu{/s} "{$sRequests.sSearch}" {s name=pim_search_sec}wurden <span
                            class="headline--product-count">{/s} {$sSearchResults.sArticlesCount + $searchResultsContent.count}{s name=pim_search_last}</span>
                    Artikel gefunden!{/s}
                </h1>
            {/block}

            {if $searchResultsContent.count gt 0 && $sSearchResults.sArticles}
                {assign var="showSearchResultTabs" value="1"}
            {else}
                {assign var="showSearchResultTabs" value="0"}
            {/if}

            {if $showSearchResultTabs}
                <div class="tab-menu--search">
                    <div class="tab--navigation">
                        <a href="#" class="tab--link has--content"
                           title="Produkte">Produkte
                            <span class="product--rating-count">{$sSearchResults.sArticlesCount}</span>
                        </a>
                        <a href="#" class="tab--link has--content {if $pPage}search{/if}" title="Inhalte">
                            Inhalte
                            <span class="product--rating-count">{$searchResultsContent.count}</span>
                        </a>
                    </div>
                    <div class="tab--container-list">
                        <div class="tab--container">
                            <div class="tab--content">
                                {block name="frontend_search_results"}
                                    <div class="search--results">
                                        {include file='frontend/listing/listing.tpl'}
                                    </div>
                                {/block}
                            </div>
                        </div>
                        <div class="tab--container">
                            <div class="tab--content">

                                {if $searchResultsContent}
                                    {foreach $searchResultsContent.result as $content}
                                        <h3>{$content.headline}</h3>
                                        <p>{$content.text}...<a
                                                    href="/{$baseContentUrl}/{$content.link}">{s name=pim_search_readmore}mehr{/s}</a></p>
                                    {/foreach}
                                {/if}
                            </div>

                            {if $searchResultsContent.pages gt 1}
                                <div class="listing--bottom-paging">
                                    {include file="frontend/listing/actions/content-pagination.tpl"}
                                </div>
                            {/if}
                        </div>
                    </div>
                </div>
            {elseif $sSearchResults.sArticles}

                {block name="frontend_search_results"}
                    <div class="search--results">
                        {include file='frontend/listing/listing.tpl'}
                    </div>
                {/block}

            {elseif $searchResultsContent.count gt 0}
                <div class="tab--content">
                    {if $searchResultsContent}
                        {foreach $searchResultsContent.result as $content}
                            <h3>{$content.headline}</h3>
                            <p>{$content.text}...<a href="/{$baseContentUrl}/{$content.link}">{s name=pim_search_readmore}mehr{/s}</a></p>
                        {/foreach}
                    {/if}
                </div>
                {if $searchResultsContent.pages gt 1}
                    <div class="listing--bottom-paging">
                        {include file="frontend/listing/actions/content-pagination.tpl"}
                    </div>
                {/if}

                {block name="frontend_search_sidebar"}
                    {include file='frontend/listing/sidebar.tpl'}
                {/block}
            {/if}
        {/if}

    </div>
{/block}