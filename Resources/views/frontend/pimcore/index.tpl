{extends file="parent:frontend/index/index.tpl"}

{block name='frontend_index_header_title'}
    {if $data.data.metadata.title|strlen gt 0}
        {strip}{$data.data.metadata.title} | {config name=sShopname}{/strip}
    {else}
        {strip}{if $sBreadcrumb}{foreach from=$sBreadcrumb|array_reverse item=breadcrumb}{$breadcrumb.name} | {/foreach}{/if}{config name=sShopname}{/strip}
    {/if}
{/block}

{block name='frontend_index_header_meta_description'}{$data.data.metadata.description}{/block}

{block name='frontend_index_header_meta_tags' prepend}
    {foreach $data.data.metadata.custom as $metadata}
        {$metadata}
    {/foreach}
{/block}

{* Breadcrumb *}
{block name='frontend_index_breadcrumb'}
    {if count($sBreadcrumb)}
        <nav class="content--breadcrumb block">
            {block name='frontend_index_breadcrumb_inner'}
                {include file='frontend/index/breadcrumb.tpl'}
            {/block}
        </nav>
    {/if}
{/block}

{block name="frontend_index_body_classes" append}
    is--ctl-custom is--ctl-pimcore
{/block}

{block name="frontend_index_left_inner" append}
    {if $catnav gt 0}
        <div class="sidebar--categories-wrapper pimcore-navigation">
            <div class="shop-sites--container content--navi is--rounded">
                <div class="shop-sites--headline navigation--headline">
                    {s name='MopePimcoreNavigationTitle'}Pimcore Navigation{/s}
                </div>
                {action module=widgets controller=PimcoreNavigation action=getNavigation catnav=$catnav documentid=$documentid}
            </div>
        </div>
    {/if}
{/block}

{block name="frontend_index_content" prepend}
    <div class="custom-page--content content block">
        <div class="content--custom">
            {include file="string:`$data.data.content`"}
        </div>
    </div>
{/block}