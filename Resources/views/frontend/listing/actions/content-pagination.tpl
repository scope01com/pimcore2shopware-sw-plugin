{namespace name="frontend/listing/listing_actions"}

<div class="listing--paging panel--paging">

    {block name='frontend_listing_actions_paging_label'}{/block}

    {block name="frontend_listing_actions_paging_first"}
        {if $pPage > 1}
            <a href="{$baseUrl}?pPage=1&sSearch={$sTerm}" title="{"{s name='ListingLinkFirst'}{/s}"|escape}" class="paging--link paging--prev" data-action-link="true">
                <i class="icon--arrow-left"></i>
                <i class="icon--arrow-left"></i>
            </a>
        {/if}
    {/block}

    {block name='frontend_listing_actions_paging_previous'}
        {if $pPage > 1}
            <a href="{$baseUrl}?pPage={$pPage-1}&sSearch={$sTerm}" title="{"{s name='ListingLinkPrevious'}{/s}"|escape}" class="paging--link paging--prev" data-action-link="true">
                <i class="icon--arrow-left"></i>
            </a>
        {/if}
    {/block}

    {block name='frontend_listing_actions_paging_numbers'}
        {if $searchResultsContent.pages > 1}
            <a title="{$sCategoryInfo.name|escape}" class="paging--link is--active">{$pPage}</a>
        {/if}
    {/block}

    {block name='frontend_listing_actions_paging_next'}
        {if $pPage < $searchResultsContent.pages}
            <a href="{$baseUrl}?pPage={$pPage+1}&sSearch={$sTerm}" title="{"{s name='ListingLinkNext'}{/s}"|escape}" class="paging--link paging--next" data-action-link="true">
                <i class="icon--arrow-right"></i>
            </a>
        {/if}
    {/block}

    {block name="frontend_listing_actions_paging_last"}
        {if $pPage < $searchResultsContent.pages}
            <a href="{$baseUrl}?pPage={$searchResultsContent.pages}&sSearch={$sTerm}" title="{"{s name='ListingLinkLast'}{/s}"|escape}" class="paging--link paging--next" data-action-link="true">
                <i class="icon--arrow-right"></i>
                <i class="icon--arrow-right"></i>
            </a>
        {/if}
    {/block}

    {block name='frontend_listing_actions_count'}
        {if $searchResultsContent.pages > 1}
            <span class="paging--display">
                {s name="ListingTextFrom"}{/s} <strong>{$searchResultsContent.pages}</strong>
            </span>
        {/if}
    {/block}

</div>