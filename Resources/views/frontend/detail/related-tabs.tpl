{block name="frontend_detail_tabs_navigation_inner" append}
    {if $relatedContent|count gt 0}
        <a title="{s name='PimcoreTagContent'}Pimcore Content{/s}" class="tab--link has--content"
           href="#">{s name='PimcoreTagContent'}Pimcore Content{/s} <span
                    class="product--rating-count">{$relatedContent|count}</span></a>
    {/if}
{/block}

{block name="frontend_detail_tabs_content_inner" append}
    {if $relatedContent|count gt 0}
        <div class="tab--container">
            <div class="tab--header">
                <a title="{s name='PimcoreTagContent'}Pimcore Content{/s}" class="tab--title"
                   href="#">{s name='PimcoreTagContent'}Pimcore Content{/s}</a>
            </div>
            <div class="tab--preview">
            </div>
            <div class="tab--content">
                <div class="content--description">
                    {foreach $relatedContent as $relatedElements}
                        {foreach $relatedElements.content as $relatedElement}
                            <div class="content--title">{$relatedElement.headline}</div>
                            <p>{$relatedElement.text}... <a title="{$relatedElement.headline}"
                                                            href="{$relatedElement.link}">{s name='PimcoreTagContentMore'}mehr{/s}</a>
                            </p>
                        {/foreach}
                    {/foreach}
                </div>

            </div>
        </div>
    {/if}
{/block}