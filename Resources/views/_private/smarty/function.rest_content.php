<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

function smarty_function_rest_content($params, &$smarty)
{
    $content = "";
    if (isset($params['documentid'])) {
        $documentid = $params['documentid'];
        unset($params['documentid']);
        $apiclient = Shopware()->Container()->get('mope_pimcore.api_client');
        $data = $apiclient->getPimcoreContent($documentid, $params);

        $content = $data['data']['content'];
    }

    return $content;
}
