//{namespace name=backend/pimcorecontent/main}
Ext.define('Shopware.apps.Emotion.view.components.MopePimcoreComponent', {
    extend: 'Shopware.apps.Emotion.view.components.Base',
    alias: 'widget.mope-pimcore',

    initComponent: function() {
        var me = this;

        me.callParent(arguments);

        me.elementFieldset.add(me.createCustomFields());

        me.pimcoreDocumentStoreField = me.getPimcoreDocumentStoreField();

        if(me.pimcoreDocumentStoreField.getValue()) {
            me.pimcoreSelect.setValue(me.pimcoreDocumentStoreField.getValue());
        }



    },

    createCustomFields: function() {
        var me = this;

        var articles = Ext.create('Ext.data.Store', {
            fields: ['name','id'],
            proxy: {
                type: 'ajax',
                url: '{url controller=PimcoreEmotion action=loadDocumentSelect}',
                reader: {
                    type:'json',
                    root: 'data'
                }
            },
            storeId: 'categories',
            root: 'data',
            autoLoad: true

        });

        me.pimcoreSelect = Ext.create('Ext.form.field.ComboBox', {
            store: articles,
            fieldLabel: 'Dokument wählen',
            labelWidth:180,
            displayField: 'name',
            valueField: 'id',
            queryMode: 'remote',
            padding: 10,
            allowBlank:true,
            required:true,
            emptyText: '{s name=documentassignment/please_choose}Bitte wählen{/s}',
            name: 'articles',
            width: 800,
            typeAhead: true,
            minChars: 2,
            listeners: {
                select: function(combo, record, index) {
                    me.pimcoreDocumentStoreField = me.getPimcoreDocumentStoreField();
                    me.pimcoreDocumentStoreField.setValue(combo.getValue());
                }
            }

        });

        return [me.pimcoreSelect];
    },

    getPimcoreDocumentStoreField: function() {
        var me = this,
            items = me.elementFieldset.items.items,
            storeField;

        Ext.each(items, function(item) {
            if(item.name === 'mopepimcore_widget_store') {
                storeField = item;
            }
        });

        return storeField;
    }
});