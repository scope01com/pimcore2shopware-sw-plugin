<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace MopePimcore\Exception;

/**
 * Class MopeApiException
 *
 * Exception class for the api
 *
 */
class MopeApiException extends \Exception
{
}
