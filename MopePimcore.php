<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace MopePimcore;

use Shopware\Components\Plugin;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Shopware\Components\Plugin\Context\ActivateContext;
use Shopware\Components\Plugin\Context\DeactivateContext;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UpdateContext;
use Shopware\Components\Plugin\Context\UninstallContext;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use MopePimcore\Models\MopeDocument;
use Doctrine\ORM\Tools\SchemaTool;

/**
 * Plugin bottstrap class
 *
 * Class MopePimcore
 * @package MopePimcore
 */
class MopePimcore extends Plugin
{

    /**
     * @param InstallContext $context
     */
    public function install(InstallContext $context)
    {
        $this->createEmotionComponent();
        try {
            $this->createSchema();
        } catch (\Exception $e) {
        }
        $this->createCategoryTextFields();
        return true;
    }

    /**
     * @param UpdateContext $context
     * @return bool
     */
    public function update(UpdateContext $context)
    {
        $this->createEmotionComponent();
        $this->createCategoryTextFields();
        $context->scheduleClearCache(InstallContext::CACHE_LIST_ALL);
        return true;
    }

    /**
     * @param ActivateContext $context
     */
    public function activate(ActivateContext $context)
    {
        $context->scheduleClearCache(InstallContext::CACHE_LIST_ALL);
        return true;
    }

    /**
     * @param DeactivateContext $context
     */
    public function deactivate(DeactivateContext $context)
    {
        $context->scheduleClearCache(InstallContext::CACHE_LIST_ALL);
        return true;
    }

    /**
     * @param UninstallContext $context
     */
    public function uninstall(UninstallContext $context)
    {
        $this->dropSchema();
        $context->scheduleClearCache(InstallContext::CACHE_LIST_ALL);
        return true;
    }

    /**
     * Build di continer with some additional params
     *
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        $container->setParameter('mope_pimcore.plugin_dir', $this->getPath());
        $container->setParameter('mope_pimcore.view_dir', $this->getPath() . '/Resources/views/');

        parent::build($container);
    }

    /**
     * Creates emotion component
     */
    public function createEmotionComponent()
    {
        /** @var \Shopware\Components\Emotion\ComponentInstaller $installer */
        $installer = $this->container->get('shopware.emotion_component_installer');

        $component = $installer->createOrUpdate(
            $this->getName(),
            'Pimcore Document',
            [
                'name' => 'Pimcore Document',
                // The name for the element
                'template' => 'mope_pimcore',
                // The name of the template file which should be used for the frontend theme.
                'xtype' => 'mope-pimcore',
                // The xtype of a custom ExtJS component which will be used for the element settings in the backend. When you set the xtype you have to provide the corresponding ExtJS component, otherwise the element will throw an error.
                'cls' => 'mope-pimcore',
                // Define a CSS class which will be used for the element template in the frontend theme.
                'description' => 'Shows content from pimcore'
                // A short description which will be shown for your element in the shopping world module.
            ]
        );
        // workaround on update, causes duplicates in the database
        $sql = 'SELECT * FROM s_library_component_field WHERE name = "mopepimcore_widget_store" AND x_type = "hiddenfield"';
        $result = Shopware()->Db()->fetchAll($sql);
        if (count($result) === 0) {
            $component->createHiddenField([
                'name' => 'mopepimcore_widget_store',
                'allowBlank' => true
            ]);
        }
    }


    /**
     * Creates database tables on base of doctrine models
     */
    private function createSchema()
    {
        $tool = new SchemaTool($this->container->get('models'));
        $classes = [
            $this->container->get('models')->getClassMetadata(MopeDocument::class)
        ];
        $tool->createSchema($classes);
    }


    /**
     * Drops database tables on base of doctrine models
     */
    private function dropSchema()
    {
        $tool = new SchemaTool($this->container->get('models'));
        $classes = [
            $this->container->get('models')->getClassMetadata(MopeDocument::class)
        ];
        $tool->dropSchema($classes);
    }

    /**
     * Creates text fields in categories
     */
    private function createCategoryTextFields()
    {
        $service = Shopware()->Container()->get('shopware_attribute.crud_service');
        $service->update(
            's_categories_attributes',
            'mope_document_assignment',
            'single_selection',
            [
                'label' => 'Pimcore Dokument zuordnung',
                'supportText' => '',
                'translatable' => false,
                'displayInBackend' => true,
                'position' => 1,
                'custom' => false,
                'entity' => \MopePimcore\Models\MopeDocument::class
            ],
            null,
            true
        );
        $service->update(
            "s_categories_attributes",
            "mope_document_assignment_show_navigation",
            "boolean",
            ['label' => 'Pimcore Unterkategorien ausgeben', 'displayInBackend' => true, 'position' => 2]
        );
        // generates attributes
        $entityManager = Shopware()->Container()->get('models');
        $entityManager->generateAttributeModels(['s_categories_attributes']);
    }
}
