<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace MopePimcore;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Message\FutureResponse;
use MopePimcore\Exception;
use Shopware\Models\Attribute\Article;
use Shopware_Components_Config;

/**
 * Simple rest api client
 *
 * Class RestApiClient
 * @package MopePimcore
 */
class RestApiClient
{
    /**
     * @var self $instance|null
     */
    protected static $instance;

    /**
     * @var GuzzleClient|null
     */
    private $client;

    /**
     * @var array $blacklist
     */
    private $blacklist;

    /**
     * @var Shopware_Components_Config $config
     */
    private $config;

    /**
     * Constructor method
     *
     * RestApiClient constructor.
     */
    public function __construct()
    {
        $client = new GuzzleClient();
        $this->client = $client;
        $this->blacklist = ['module', 'controller', 'action'];
        $this->config = Shopware()->Config();
    }

    /**
     * Prevent object clone
     *
     * @throws \Exception
     */
    public function __clone()
    {
        throw new \Exception("Can't clone singleton object");
    }

    /**
     * Returns an instance of this class.
     *
     * @return RestApiClient
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Gets resource by path from pimcore
     *
     * @param string $sUrl
     * @param array $params
     * @return mixed
     */
    public function getPimcoreContent($sUrl, $params = [])
    {
        $debug = $this->config->getByNamespace('MopePimcore', 'debug');
        $logger = Shopware()->Container()->get('pluginlogger');
        $apiUrl = $this->config->getByNamespace('MopePimcore', 'apiurl');
        $apiKey = $this->config->getByNamespace('MopePimcore', 'apikey');
        $language = $this->config->getByNamespace('MopePimcore', 'language');

        $params['path'] = $sUrl;
        $params['language'] = $language;

        $uri = $apiUrl . "/cms/webcontent?apikey=" . $apiKey;
        $requestParams = Shopware()->Front()->Request()->getParams();
        $additionalParams = array_merge($requestParams, $params);
        $formParams = $this->_filterParams($additionalParams);
        if ((int)$debug === 1) {
            $logger->info($uri);
            $logger->info(\print_r($formParams, true));
        }

        /** @var FutureResponse $response */
        $response = $this->client->post($uri, [
            'body' => [
                    $formParams
                ]
        ]);

        if ((int)$debug === 1) {
            $logger->info(\print_r($response, true));
        }

        $data = $response->json();
        $data["data"] = $this->_prepareContent($data["data"]);

        $data["data"]["breadcrumb"] = $this->prepareBreadCrumb($data["data"]["breadcrumb"]);

        return $data;
    }

    /**
     * Get request to the rest api
     *
     * @param string $endpoint
     * @param array $params
     * @return string json
     * @throws Exception\MopeApiException
     */
    public function get($endpoint, $params = [])
    {
        $logger = Shopware()->Container()->get('pluginlogger');
        $apiUrl = $this->config->getByNamespace('MopePimcore', 'apiurl');
        $apiKey = $this->config->getByNamespace('MopePimcore', 'apikey');
        $language = $this->config->getByNamespace('MopePimcore', 'language');
        $baseUrl = $this->config->getByNamespace('MopePimcore', 'baseurl');
        $paramString = "";
        foreach ($params as $key => $param) {
            $paramString .= "&" . $key . "=" . $param;
        }

        $uri = $apiUrl . "/cms/" . $endpoint . "?apikey=" . $apiKey . "&language=" . $language . "&baseurl=" . $baseUrl . $paramString;
        try {
            /** @var FutureResponse $response */
            $response = $this->client->get($uri);
            $debug = $this->config->getByNamespace('MopePimcore', 'debug');
            if ((int)$debug === 1) {
                $logger->info($uri);
                $logger->info(print_r($response, true));
            }
            $responseMessage = $response->json();
        } catch (\Exception $e) {
            throw new Exception\MopeApiException('Something wrong: ' . $e->getMessage());
        }


        return $responseMessage;
    }

    /**
     * Filter params, before send request
     *
     * @param array $params
     * @return array $params
     */
    private function _filterParams($params)
    {
        foreach ($params as $paramKey => $param) {
            if (\in_array($paramKey, $this->blacklist, true)) {
                unset($params[$paramKey]);
            }
        }
        return $params;
    }


    /**
     * Prepares content for output, replaces url placeholder etc.
     *
     * @param array $content
     * @return mixed
     */
    private function _prepareContent($content)
    {
        $baseUrl = $this->config->getByNamespace('MopePimcore', 'baseurl');
        $pimcoreUrl = Shopware()->Front()->Request()->getBaseUrl() . "/" . $baseUrl;
        $content['content'] = str_replace('{SHOP_URL}', $pimcoreUrl, $content['content']);
        $content['content'] = str_replace('http://placeholder', '', $content['content']);

        $regexp = '/intId[a-f0-9]{32}/i';
        preg_match_all($regexp, $content['content'], $aMatches);
        foreach ($aMatches as $aMatchItems) {
            foreach ($aMatchItems as $aMatch) {
                $internalId = str_replace('intId', '', $aMatch);
                $query = ['mopeInternalid' => $internalId];
                /** @var Article $prodAttribute */
                $prodAttribute = Shopware()->Models()->getRepository(Article::class)->findOneBy($query);
                if ($prodAttribute && $prodAttribute->getArticle()->getId() > 0) {
                    $articleDetailid = $prodAttribute->getArticle()->getId();

                    $content['content'] = str_replace($aMatch, $articleDetailid, $content['content']);
                }
            }
        }

        return $content;
    }

    /**
     * Prepares breadcrumb url
     *
     * @param array $content
     * @return array $content
     */
    public function prepareBreadCrumb($content)
    {
        $baseUrl = $this->config->getByNamespace('MopePimcore', 'baseurl');
        $pimcoreUrl = Shopware()->Front()->Request()->getBaseUrl() . "/" . $baseUrl;
        foreach ($content as $key => $item) {
            $content[$key]['link'] = $pimcoreUrl . $item['link'];
        }

        return $content;
    }

    /**
     * Prepares navigation, replace base url and add additional classes
     *
     * @param string $content
     * @return string $content
     */
    public function prepareNavigation($content)
    {
        $regexp = '/(<a [^>]+>)/i';
        $baseUrl = $this->config->getByNamespace('MopePimcore', 'baseurl');
        $language = $this->config->getByNamespace('MopePimcore', 'language');
        $iResults = \preg_match_all($regexp, $content, $aMatches);
        foreach ($aMatches[0] as $a) {
            if (preg_match('/href="([^"]+)/', $a, $matches)) {
                $link = $matches[1];
                $originLink = $matches[1];
                $link = \str_replace('/' . $language, '', $link);
                $newLink = '/' . $baseUrl . $link;
                $content = \str_replace('"' . $originLink . '"', '"' . $newLink . '"', $content);
            }
            if (preg_match('/class="([^"]+)/', $a, $matches)) {
                $class = $matches[1];
                $newClass = 'navigation--link';
                $content = \str_replace('class="' . $class . '"', 'class="' . $newClass . '"', $content);
            }
        }
        return $content;
    }
}
