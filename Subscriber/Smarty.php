<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace MopePimcore\Subscriber;

use Enlight\Event\SubscriberInterface;

/**
 * Class Smarty
 * @package MopePimcore\Subscriber
 */
class Smarty implements SubscriberInterface
{
    /** @var string  */
    private $viewDir;


    /**
     * @param string $viewDir
     */
    public function __construct($viewDir)
    {
        $this->viewDir = $viewDir;
    }

    /**
     * Subscribe events
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'Theme_Inheritance_Smarty_Directories_Collected' => 'onThemeInheritanceSmartyCollectedFilter'
        ];
    }

    /**
     * @param \Enlight_Event_EventArgs $args
     * @return array|mixed
     */
    public function onThemeInheritanceSmartyCollectedFilter(\Enlight_Event_EventArgs $args)
    {
        $result = $args->getReturn();

        $result[] = $this->viewDir . '_private/smarty/';

        return $result;
    }
}
