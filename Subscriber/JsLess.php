<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace MopePimcore\Subscriber;

use Enlight\Event\SubscriberInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class JsLess
 * @package MopePimcore\Subscriber
 */
class JsLess implements SubscriberInterface
{

    /** @var string  */
    private $pluginDir;
    /** @var string  */
    private $viewDir;
    /** @var \Enlight_Template_Manager  */
    private $template;

    /**
     * Subscribe events
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'Theme_Compiler_Collect_Plugin_Less'  => 'addLessFiles',
            'Theme_Compiler_Collect_Plugin_Javascript' => 'addJsFiles',
        ];
    }

    /**
     * @param string $pluginDir
     * @param string $viewDir
     * @param \Enlight_Template_Manager $template
     */
    public function __construct(
        $pluginDir,
        $viewDir,
        \Enlight_Template_Manager $template
    ) {
        $this->pluginDir = $pluginDir;
        $this->viewDir = $viewDir;
        $this->template = $template;
    }

    /**
     * Provide the file collection for less
     *
     * @param \Enlight_Event_EventArgs $args
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function addLessFiles(\Enlight_Event_EventArgs $args)
    {
        $less = new \Shopware\Components\Theme\LessDefinition(
            [],
            [
                $this->pluginDir . '/Resources/views/frontend/public/src/less/pimcore.less'
            ],
            //import directory
            __DIR__
        );

        return new ArrayCollection([$less]);
    }

    /**
     * Provide the file collection for js files
     *
     * @param \Enlight_Event_EventArgs $args
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function addJsFiles(\Enlight_Event_EventArgs $args)
    {
        $jsFiles = [
            $this->pluginDir . '/Resources/views/frontend/public/src/js/pimcore.js',
        ];
        return new ArrayCollection($jsFiles);
    }
}
