<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace MopePimcore\Subscriber;

use Enlight\Event\SubscriberInterface;

/**
 * Class Router
 * @package MopePimcore\Subscriber
 */
class Router implements SubscriberInterface
{

    /**
     * Subscribe events
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Front_RouteShutdown' => 'onRouterRouteShutdown'
        ];
    }


    /**
     * Router constructor.
     */
    public function __construct()
    {
    }

    /**
     * On router route shutdown, checks in configuration fo base path
     * If match redirect to pimcore controller
     *
     * @param \Enlight_Controller_EventArgs $args
     */
    public function onRouterRouteShutdown(\Enlight_Controller_EventArgs $args)
    {
        $request = $args->getRequest();
        $baseUrl = Shopware()->Config()->getByNamespace('MopePimcore', 'baseurl');
        if ($request->getControllerName() === $baseUrl) {
            $request->setControllerName('pimcore');
        }
    }
}
