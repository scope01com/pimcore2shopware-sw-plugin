<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace MopePimcore\Subscriber;

use Enlight\Event\SubscriberInterface;
use MopePimcore\Exception\MopeApiException;
use MopePimcore\Models\MopeDocument;
use Psr\Container\ContainerInterface;
use Shopware\Bundle\StoreFrontBundle\Struct\ShopContext;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Category\Category;

/**
 * Class Controller
 * @package MopePimcore\Subscriber
 */
class Controller implements SubscriberInterface
{
    /** @var string  */
    private $pluginDir;
    /** @var string  */
    private $viewDir;
    /** @var \Enlight_Template_Manager  */
    private $template;
    /** @var \Shopware_Components_Config|null  */
    private $config;
    /** @var ContainerInterface  */
    private $container;
    /** @var ModelManager  */
    private $em;

    /**
     * Subscribe events
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Dispatcher_ControllerPath_Frontend_Pimcore' => 'registerController',
            'Enlight_Controller_Dispatcher_ControllerPath_Widgets_PimcoreNavigation' => 'registerNavigationController',
            'Enlight_Controller_Dispatcher_ControllerPath_Api_Webhooks' => 'registerWebhooksController',
            'Enlight_Controller_Action_PostDispatchSecure_Backend_Emotion' => 'onPostDispatchBackendEmotion',
            'Enlight_Controller_Action_PreDispatch_Frontend_Listing' => 'onPreDispatchListing',
            'Enlight_Controller_Action_PostDispatch_Frontend_Search' => 'onPostDispatchFrontendSearch',
            'Enlight_Controller_Action_PostDispatchSecure_Frontend' => 'onPostDispatchSecureFrontend',
            'Enlight_Controller_Action_PostDispatchSecure_Frontend_Detail' => 'onPostDispatchSecureDetailFrontend'
        ];
    }

    /**
     * @param string $pluginDir
     * @param string $viewDir
     * @param \Enlight_Template_Manager $template
     */
    public function __construct(
        $pluginDir,
        $viewDir,
        \Enlight_Template_Manager $template
    ) {
        $this->pluginDir = $pluginDir;
        $this->viewDir = $viewDir;
        $this->template = $template;
        $this->config = Shopware()->Config();
        $this->container = Shopware()->Container();
        $this->em = Shopware()->Models();
    }

    /**
     * Register controller
     *
     * @param \Enlight_Event_EventArgs $args
     * @return string
     */
    public function registerController(\Enlight_Event_EventArgs $args)
    {
        return $this->pluginDir . '/Controllers/Frontend/Pimcore.php';
    }

    /**
     * Register navigation controller
     *
     * @param \Enlight_Event_EventArgs $args
     * @return string
     */
    public function registerNavigationController(\Enlight_Event_EventArgs $args)
    {
        return $this->pluginDir . '/Controllers/Widgets/PimcoreNavigation.php';
    }

    /**
     * Register controller
     *
     * @param \Enlight_Event_EventArgs $args
     * @return string
     */
    public function registerWebhooksController(\Enlight_Event_EventArgs $args)
    {
        return $this->pluginDir . '/Controllers/Api/Webhooks.php';
    }

    /**
     * Extends the backend template to add the grid component for the emotion designer.
     *
     * @param \Enlight_Controller_ActionEventArgs $args
     */
    public function onPostDispatchBackendEmotion(\Enlight_Controller_ActionEventArgs $args)
    {
        $controller = $args->getSubject();
        /** @var \Enlight_View_Default $view */
        $view = $controller->View();
        $view->addTemplateDir($this->viewDir);
        $view->extendsTemplate('backend/emotion/mope_pimcore/view/detail/elements/mope_pimcore.js');
    }

    /**
     * Pre dispatch listing, display content assignment
     *
     * @param \Enlight_Controller_ActionEventArgs $args
     */
    public function onPreDispatchListing(\Enlight_Controller_ActionEventArgs $args)
    {
        $controller = $args->getSubject();
        /** @var \Enlight_View_Default $view */
        $view = $controller->View();
        $view->addTemplateDir($this->viewDir);
        $requestParams = $args->getSubject()->Request()->getParams();
        $categoryId = $requestParams["sCategory"];

        if ($categoryId > 0) {
            /** @var Category|null $category */
            $category = $this->em->getRepository(Category::class)->find($categoryId);
            if (null !== $category && null !== $category->getAttribute()) {
                /** @var MopeDocument $assignment */
                $assignment = $category->getAttribute()->getMopeDocumentAssignment();
                if (null !== $assignment) {
                    /** @var MopeDocument|null $assignemntObject */
                    $assignemntObject = $this->em->getRepository(MopeDocument::class)->find($assignment);
                    if ($assignemntObject) {
                        $documentId = $assignemntObject->getDocumentid();
                        if ($documentId > 0) {
                            $params = [
                                "documentid" => $documentId,
                                "categoryid" => $categoryId
                            ];
                            $args->getSubject()->forward("index", "pimcore", null, $params);
                        }
                    }
                }
            }
        }
    }


    /**
     * Executes content search in pimcore
     *
     * @param \Enlight_Controller_ActionEventArgs $args
     * @throws MopeApiException
     */
    public function onPostDispatchFrontendSearch(\Enlight_Controller_ActionEventArgs $args)
    {
        $search = (int)$this->config->getByNamespace('MopePimcore', 'search');
        $perpage = (int)$this->config->getByNamespace('MopePimcore', 'perpage');
        $baseUrl = $this->config->getByNamespace('MopePimcore', 'baseurl');

        if ($search === 1) {
            $controller = $args->getSubject();
            /** @var \Enlight_View_Default $view */
            $view = $controller->View();
            $request = $controller->Request();
            $searchTerm = $request->get('sSearch', '');
            $page = (int)$request->get('pPage');
            if ($page === 0) {
                $page = 1;
            }

            $response = null;
            if ('' !== $searchTerm) {
                $searchParams = [
                    'searchterm' => $searchTerm,
                    'page' => $page,
                    'resultsperpage' => $perpage
                ];

                /** @var \MopePimcore\RestApiClient $apiclient */
                $apiclient = $this->container->get('mope_pimcore.api_client');
                /** @var array $response */
                $response = $apiclient->get('search', $searchParams);
            }
            if (\is_array($response) && $response['success'] === true && \count($response['data']['result']['result']) > 0) {
                $view->addTemplateDir(__DIR__ . '/../Resources/views/');
                $view->extendsTemplate('frontend/search/pim_fuzzy.tpl');
                $view->assign('searchResultsContent', $response['data']['result']);
                $view->assign('sTerm', $searchTerm);
                $view->assign('pPage', $page);
                $view->assign('baseContentUrl', $baseUrl);
            }
        }
    }

    /**
     * Post dispatch frontend
     * Builds top menu
     *
     * @param \Enlight_Controller_ActionEventArgs $args
     * @throws MopeApiException
     */
    public function onPostDispatchSecureFrontend(\Enlight_Controller_ActionEventArgs $args)
    {
        /** @var \Enlight_View_Default $view */
        $view = $args->getSubject()->View();
        $categories = $this->em->getRepository(\Shopware\Models\Attribute\Category::class)->findBy(['mopeDocumentAssignmentShowNavigation' => 1]);
        foreach ($categories as $category) {
            $document = $this->em->getRepository(\MopePimcore\Models\MopeDocument::class)->find($category->getMopeDocumentAssignment());
            if ($document) {
                $data[$category->getCategoryId()] = [
                    'shownavigation' => $category->getMopeDocumentAssignmentShowNavigation(),
                    'documentid' => $document->getDocumentid(),
                ];
            }
        }

        $menu = $view->getAssign('sAdvancedMenu');
        if (isset($data)) {
            $menu = $this->buildTopNav($menu, $data);
        }

        $view->assign('sAdvancedMenu', $menu);
    }

    /**
     * Post dispatch detail page, adds tag content to assignment
     *
     * @param \Enlight_Controller_ActionEventArgs $args
     */
    public function onPostDispatchSecureDetailFrontend(\Enlight_Controller_ActionEventArgs $args)
    {
        $logger = $this->container->get('pluginlogger');
        /** @var \MopePimcore\RestApiClient $apiclient */
        $apiclient = $this->container->get('mope_pimcore.api_client');
        $params['productid'] = $args->getSubject()->Request()->sArticle;
        try {
            /** @var array $relationContent */
            $relationContent = $apiclient->get('relationdata', $params);
            /** @var \Enlight_View_Default $view */
            $view = $args->getSubject()->View();
            $view->assign('relatedContent', $relationContent['data']['relationdata']);
            $view->addTemplateDir(__DIR__ . '/../Resources/views/');
            $view->extendsTemplate('frontend/detail/related-tabs.tpl');
        } catch (MopeApiException $e) {
            $logger->error($e->getMessage());
        }
    }

    /**
     * Builds top navigation recursive
     *
     * @param array $navList
     * @param array $data
     * @return array
     *
     * @throws MopeApiException
     */
    private function buildTopNav($navList, $data)
    {
        $logger = $this->container->get('pluginlogger');
        $debug = $this->config->getByNamespace('MopePimcore', 'debug');
        /** @var ShopContext $context */
        $context = $this->container->get('shopware_storefront.context_service')->getShopContext();
        $cache = $this->container->get('cache');
        $cacheOn = $this->config->getByNamespace('MopePimcore', 'cache');
        $cacheTime = $this->config->getByNamespace('MopePimcore', 'cachetime');
        $result = [];
        foreach ($navList as $key => $menuItem) {
            $menuItemTemp = [];
            if (array_key_exists($menuItem['id'], $data)) {
                /** @var \MopePimcore\RestApiClient $apiclient */
                $apiclient = $this->container->get('mope_pimcore.api_client');
                $params['documentid'] = $data[$menuItem['id']]['documentid'];
                $params['c'] = $menuItem['id'];

                $cacheKey = 'Pimcore_Navigation_Top_' . $context->getShop()->getId() . '_' . $params['documentid'] . '_' . $key;
                if ((int)$debug === 1) {
                    $logger->info('fetch navigation data, current document id : ' . (int)$params['documentid']);
                    $logger->info('fetch navigation data, cache key: ' . $cacheKey);
                }

                if ((int)$cacheOn === 1 && $cache->test($cacheKey)) {
                    $dataNav = $cache->load($cacheKey);
                    if ((int)$debug === 1) {
                        $logger->info('fetch navigation data, load from cache: ' . $cacheKey);
                    }
                } else {
                    /** @var array  $navData */
                    $navData = $apiclient->get('navigationdata', $params);
                    $dataNav = $navData['data']['navigation'];
                    $cache->save($dataNav, $cacheKey, ['Pimcore_Navigation'], $cacheTime);
                    if ((int)$debug === 1) {
                        $logger->info('fetch navigation data, fetch via api : ' . $cacheKey);
                    }
                }

                $menuItemTemp = $dataNav;
            }
            if (count($menuItem['sub']) > 0) {
                $menuItem['sub'] = $this->buildTopNav($menuItem['sub'], $data);
                $menuItem['sub'] = \array_merge($menuItem['sub'], $menuItemTemp);
            }
            $result[] = $menuItem;
        }
        return $result;
    }
}
