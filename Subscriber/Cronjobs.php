<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace MopePimcore\Subscriber;

use Enlight\Event\SubscriberInterface;
use Shopware\Components\Model\ModelManager;

/**
 * Class Cronjobs
 *
 * @package MopePimcore\Subscriber
 */
class Cronjobs implements SubscriberInterface
{
    /**
     * Subscribe events
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        /**
         * 1. Sync documents to shopware
         */
        return [
            'Shopware_CronJob_SyncPimcoreDocuments' => 'onRunSyncPimcoreDocuments'
        ];
    }

    /**
     * Sync documents to shopware
     *
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \MopePimcore\Exception\MopeApiException
     * @throws \Zend_Db_Adapter_Exception
     */
    public function onRunSyncPimcoreDocuments()
    {
        /** @var \MopePimcore\RestApiClient $apiclient */
        $apiclient = Shopware()->Container()->get('mope_pimcore.api_client');
        /** @var ModelManager $em */
        $em = Shopware()->Models();
        /** @var array $data */
        $data = $apiclient->get('documentselect');
        $updatedIds = [];
        foreach ($data['data']['data'] as $select) {
            $updatedIds[] = $select['id'];
            /** @var \MopePimcore\Models\MopeDocument|null $document */
            $document = $em->getRepository(\MopePimcore\Models\MopeDocument::class)->findOneBy(['documentid' => $select['id']]);
            if ($document) {
                $document->setName($select['name'] . "(" . $select['id'] . ")");
                $em->persist($document);
            } else {
                $document = new \MopePimcore\Models\MopeDocument();
                $document->setDocumentid($select['id']);
                $document->setName($select['name'] . "(" . $select['id'] . ")");
                $em->persist($document);
            }

            $em->flush();
        }

        // cleanup not existing
        $sql = "DELETE FROM mope_document_assignment WHERE documentid NOT IN ( '" . \implode("', '", $updatedIds) . "' )";
        Shopware()->Db()->query($sql);
        return true;
    }
}
