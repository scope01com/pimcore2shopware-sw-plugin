<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

/**
 * Class Shopware_Controllers_Widgets_PimcoreNavigation
 *
 * Widget controller for pimcore navigation
 */
class Shopware_Controllers_Widgets_PimcoreNavigation extends Enlight_Controller_Action
{

    /**
     * Gets pimcore navigation from cache or via rest api
     * Include as esi
     *
     * @throws Zend_Cache_Exception
     * @throws \MopePimcore\Exception\MopeApiException
     * @throws \Exception
     */
    public function getNavigationAction()
    {
        /** @var \Shopware_Components_Config $config */
        $config = Shopware()->Config();
        $logger = $this->container->get('pluginlogger');
        $debug = $config->getByNamespace('MopePimcore', 'debug');
        $this->Front()->Plugins()->ViewRenderer()->setNoRender();
        $catNav = $this->Request()->getParam('catnav');
        $documentId = $this->Request()->getParam('documentid');

        /** @var \Shopware\Bundle\StoreFrontBundle\Struct\ShopContext $context */
        $context = $this->container->get('shopware_storefront.context_service')->getShopContext();
        $cacheKey = 'Pimcore_Navigation_' . $context->getShop()->getId() . '_' . $catNav . '_' . $documentId;
        $cache = $this->container->get('cache');
        $cacheOn = $config->getByNamespace('MopePimcore', 'cache');
        $cacheTime = $config->getByNamespace('MopePimcore', 'cachetime');
        $navigation = "";

        if ((int)$debug === 1) {
            $logger->info('fetch navigation data, navigation document id : ' . (int)$catNav);
            $logger->info('fetch navigation data, current document id : ' . (int)$documentId);
            $logger->info('fetch navigation data, cache key: ' . $cacheKey);
        }
        if ((int)$catNav > 0) {
            if ((int)$cacheOn === 1 && $cache->test($cacheKey)) {
                $navigation = $cache->load($cacheKey);
                if ((int)$debug === 1) {
                    $logger->info('fetch navigation data, load from cache: ' . $cacheKey);
                }
            } else {
                /** @var \MopePimcore\RestApiClient $apiclient */
                $apiclient = $this->container->get('mope_pimcore.api_client');
                $params['catnav'] = $catNav;
                $params['documentid'] = $documentId;
                /** @var array $navData */
                $navData = $apiclient->get('navigation', $params);
                $navigation = $apiclient->prepareNavigation($navData['data']['navigation']);
                $cache->save($navigation, $cacheKey, ['Pimcore_Navigation'], $cacheTime);
                if ((int)$debug === 1) {
                    $logger->info('fetch navigation data, fetch via api : ' . $cacheKey);
                }
            }
        }

        echo $navigation;
    }
}
