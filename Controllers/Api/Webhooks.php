<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

/**
 * Class Shopware_Controllers_Api_Webhooks
 *
 * Api controller provides web hooks
 */
class Shopware_Controllers_Api_Webhooks extends Shopware_Controllers_Api_Rest
{

    /**
     * Invalidates document cache
     * Gets locales result
     *
     * GET /api/webhooks/{id}
     *
     * @throws Enlight_Event_Exception
     * @throws Zend_Cache_Exception
     * @throws \Exception
     */
    public function putAction()
    {
        $logger = $this->get('pluginlogger');
        $debug = Shopware()->Config()->getByNamespace('MopePimcore', 'debug');
        $params = $this->Request()->getPost('cacheids');
        $cacheId = $this->Request()->getParam('id');
        /** @var Enlight_View_Default $view */
        $view = $this->View();
        if ($cacheId) {
            $fullid = 'pimcoredocument' . $cacheId;
            if ((int)$debug === 1) {
                $logger->info('invalidate cache id: ' . $fullid);
            }
            Shopware()->Events()->notify(
                'Shopware_Plugins_HttpCache_InvalidateCacheId',
                [
                    'cacheId' => $fullid,
                ]
            );

            foreach ($params as $docId) {
                $cacheEntry = 'pimcoredocument' . $docId;
                $logger->info('invalidate cache id: ' . $cacheEntry);
                Shopware()->Events()->notify(
                    'Shopware_Plugins_HttpCache_InvalidateCacheId',
                    [
                        'cacheId' => $cacheEntry,
                    ]
                );
            }

            // clean up navigation cache
            $cache = Shopware()->Container()->get('cache');
            $cache->clean('matchingTag', ['Pimcore_Navigation']);

            $view->assign('success', true);
            $view->assign('cacheId', $fullid);
        } else {
            if ((int)$debug === 1) {
                $logger->info('no cache id given, nothing to invalidate');
            }
            $view->assign('success', false);
            $view->assign('cacheId', 'no cache id given');
        }
    }
}
