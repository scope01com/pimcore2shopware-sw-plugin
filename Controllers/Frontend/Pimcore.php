<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

/**
 * Class Shopware_Controllers_Frontend_Pimcore
 *
 * Frontend controller class, provides frontend logic
 */
class Shopware_Controllers_Frontend_Pimcore extends \Enlight_Controller_Action
{
    /** @var \Shopware\Components\Model\ModelManager */
    private $em;
    /** @var \MopePimcore\RestApiClient apiclient */
    private $apiclient;
    /** @var \Shopware_Components_Config */
    private $config;

    /**
     * Pre dispatch action, redirect to index action if action is not index
     */
    public function preDispatch()
    {
        // adds template directory
        $this->container->get('Template')->addTemplateDir(
            __DIR__ . '/../../Resources/views/'

        );
        // forward to action
        if ($this->Request()->getActionName() !== 'index') {
            $this->forward('index');
        }
        $this->em = Shopware()->Models();
        $this->apiclient = $this->container->get('mope_pimcore.api_client');
        $this->config = Shopware()->Config();
    }

    /**
     * Helper function to enable the http cache for a single shopware controller.
     *
     * @param int $cacheTime
     * @param array $cacheIds
     * @throws Enlight_Event_Exception
     */
    public function enableControllerCache($cacheTime = 3600, $cacheIds = []): void
    {
        /** @var Enlight_Controller_Front $front */
        $front = Shopware()->Front();
        $front->Response()->setHeader(
            'Cache-Control',
            'public, max-age=' . $cacheTime . ', s-maxage=' . $cacheTime,
            true
        );
        $this->setCacheIdHeader($cacheIds);
    }

    /**
     * Helper function to flag the request with cacheIds
     * to invalidate the caching.
     *
     * @param array $cacheIds
     * @throws Enlight_Event_Exception
     */
    public function setCacheIdHeader($cacheIds = []): void
    {
        /** @var Enlight_Controller_Front $front */
        $front = Shopware()->Front();
        $cacheIds = Shopware()->Events()->filter(
            'Shopware_Plugins_HttpCache_GetCacheIds',
            $cacheIds,
            ['subject' => $this, 'action' => 'index']
        );

        if (empty($cacheIds)) {
            return;
        }

        $cacheIdsStr = ';' . \implode(';', $cacheIds) . ';';
        $front->Response()->setHeader('x-shopware-cache-id', $cacheIdsStr);
    }

    /**
     * Index action of pimcore controller
     * Shows pimcore data on te page
     *
     * @throws Enlight_Event_Exception
     * @throws Zend_Cache_Exception
     * @throws \MopePimcore\Exception\MopeApiException
     */
    public function indexAction(): void
    {
        /** @var Enlight_View_Default $view */
        $view = $this->View();
        try {
            $logger = $this->get('pluginlogger');
        } catch (\Exception $e) {
            $logger = new \Psr\Log\NullLogger();
        }
        /** @var Enlight_Controller_Request_RequestHttp $request */
        $request = $this->Request();

        $debug = (int)$this->config->getByNamespace('MopePimcore', 'debug');
        $url = $request->getRequestUri();
        $reqUrl = $request->getRequestUri();
        $baseUrl = $this->config->getByNamespace('MopePimcore', 'baseurl');
        $url = \str_replace($request->getBaseUrl() . "/" . $baseUrl, "", $url);
        $navCat = $this->config->getByNamespace('MopePimcore', 'navcat');
        $documentId = $request->get('documentid');
        $categoryId = $request->get('categoryid');

        $explodedUrl = \explode('?', $url);
        $url = $explodedUrl[0];
        $objectResolved = false;

        if ($documentId > 0 || \strpos($reqUrl, $baseUrl) !== false) {
            $urlArray = \explode('/', $url);
            $objectNameWithId = \end($urlArray);
            $objectId = \explode('_', $objectNameWithId);
            if (isset($objectId[1])) {
                /** @var array $response */
                $response = $this->apiclient->get('resolveobject', ['id' => $objectId[1]]);
                if ($response['success'] === true && $response['data']['objectid'] > 0) {
                    $objectResolved = true;
                    $url = \str_replace($objectNameWithId, '', $url);
                }
            }
            // try to load content
            try {
                if ($objectResolved === true && isset($response)) {
                    $data = $this->apiclient->getPimcoreContent($url, ['objectid' => $response['data']['objectid'], 'categoryId' => $categoryId]);
                } else {
                    if ($documentId > 0) {
                        $url = $documentId;
                    }
                    $data = $this->apiclient->getPimcoreContent($url, ['categoryId' => $categoryId]);
                }
            } catch (\Exception $e) {
                if ($debug === 1) {
                    $logger->error($e->getMessage());
                    $logger->error($this->Request()->getRequestUri());
                }
                $url = $this->Front()->Router()->assemble([
                    'module' => 'frontend',
                    'controller' => 'index'
                ]);
                $this->redirect($url, ['code' => 301]);
            }

            // assign data
            if (isset($data) && \is_array($data)) {
                $view->assign('data', $data);
                $view->assign('sBreadcrumb', $data["data"]["breadcrumb"]);
                $view->assign('products', $data['data']['products']);
                $view->assign('catnav', $navCat);
                $view->assign('documentid', $data["data"]["documentid"]);

                $parent = Shopware()->Shop()->get('parentID');
                $categoryId = $this->Request()->getParam('sCategory', $parent);

                $categories = Shopware()->Modules()->Categories()->sGetCategories($categoryId);

                $this->fillUpNavigation($categories, $categoryId, $data["data"]["documentid"]);

                $view->assign("categoriespimcore", $categories);

                $cache = $this->config->getByNamespace('MopePimcore', 'cache');
                $cacheTime = $this->config->getByNamespace('MopePimcore', 'cachetime');
                if ((int)$cache === 1) {
                    $this->enableControllerCache($cacheTime, ['pimcoredocument' . $data["data"]['documentid']]);
                }
            }
        } elseif ($debug === 1) {
            $logger->error('request base url dont match request. requesturl: ' . $reqUrl . ' baseurl: ' . $baseUrl);
        }
    }

    /**
     * Fills shopware navigation with pimcore docs
     * Creates mixed navigation
     *
     * @param array $categories
     * @param int $currentCat
     * @param int $activeDoc
     * @throws Zend_Cache_Exception
     * @throws \MopePimcore\Exception\MopeApiException
     */
    public function fillUpNavigation(&$categories, $currentCat, $activeDoc)
    {
        $logger = $this->container->get('pluginlogger');
        $debug = $this->config->getByNamespace('MopePimcore', 'debug');
        $context = $this->container->get('shopware_storefront.context_service')->getShopContext();
        $cache = $this->container->get('cache');
        $cacheOn = $this->config->getByNamespace('MopePimcore', 'cache');
        $cacheTime = $this->config->getByNamespace('MopePimcore', 'cachetime');
        foreach ($categories as $key => $category) {
            $id = $category["id"];

            if ((int)$currentCat === $id) {
                $loadedCat = $this->em->getRepository(\Shopware\Models\Category\Category::class)->find($id);
                if ($loadedCat) {
                    /** @var \Shopware\Models\Attribute\Category $catAttribute */
                    $catAttribute = $loadedCat->getAttribute();
                    if (null !== $catAttribute) {
                        $assignment = $catAttribute->getMopeDocumentAssignment();
                        $showNav = $catAttribute->getMopeDocumentAssignmentShowNavigation();

                        if ($assignment && (int)$showNav === 1) {
                            $assignemntObject = $this->em->getRepository(\MopePimcore\Models\MopeDocument::class)->find($assignment);
                            if ($assignemntObject) {
                                $documentId = $assignemntObject->getDocumentid();
                                if ((int)$documentId > 0) {
                                    $params['documentid'] = $documentId;
                                    $params['c'] = $currentCat;
                                    $params['activedocumentid'] = $activeDoc;

                                    $cacheKey = 'Pimcore_Navigation_Side_' . $context->getShop()->getId() . '_' . $params['documentid'] . '_' . $activeDoc . '_' . $key;

                                    if ((int)$debug === 1) {
                                        $logger->info('fetch navigation data, current document id : ' . (int)$activeDoc);
                                        $logger->info('fetch navigation data, cache key: ' . $cacheKey);
                                    }
                                    if ((int)$cacheOn === 1 && $cache->test($cacheKey)) {
                                        if ((int)$debug === 1) {
                                            $logger->info('fetch navigation data, load from cache: ' . $cacheKey);
                                        }
                                        $dataNav = $cache->load($cacheKey);
                                    } else {
                                        /** @var array $navData */
                                        $navData = $this->apiclient->get('navigationdata', $params);
                                        $dataNav = $navData['data']['navigation'];
                                        $cache->save($dataNav, $cacheKey, ['Pimcore_Navigation'], $cacheTime);
                                        if ((int)$debug === 1) {
                                            $logger->info('fetch navigation data, fetch via api : ' . $cacheKey);
                                        }
                                    }

                                    $menuItemTemp = $dataNav;

                                    $categories[$key]["subcategories"] = \array_merge($categories[$key]["subcategories"], $menuItemTemp);
                                }
                            }
                        }
                    }
                }
            }
            if (count($category["subcategories"]) > 0) {
                $this->fillUpNavigation($categories[$key]["subcategories"], $currentCat, $activeDoc);
            }
        }
    }
}
