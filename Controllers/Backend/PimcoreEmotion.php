<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

class Shopware_Controllers_Backend_PimcoreEmotion extends Shopware_Controllers_Backend_ExtJs
{

    /**
     * this function is called initially and extends the standard template
     * directory
     *
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->view->addTemplateDir(__DIR__ . "/views/");
    }
   

    /**
     * This function loads pimcore content via api
     *
     * @throws \MopePimcore\Exception\MopeApiException
     */
    public function loadDocumentSelectAction()
    {
        /** @var \MopePimcore\RestApiClient $apiclient */
        $apiclient = $this->container->get('mope_pimcore.api_client');
        /** @var array  $aSelect */
        $aSelect = $apiclient->get('documentselect');
        if (\count($aSelect['data']['data'])) {
            foreach ($aSelect['data']['data'] as $key => $item) {
                $name = $item['name'] . '(' . $item['id'] . ')';
                $aSelect['data']['data'][$key]['name'] = $name;
            }
        }
        $this->view->assign([
            'success' => true,
            'data' => $aSelect['data']['data'],
            'totalCount' => count($aSelect['data']['data'])
        ]);
    }
}
