<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace MopePimcore\Models;

use Shopware\Components\Model\ModelRepository;

/**
 * Repository for the address model (MopePimcore\Models\MopeDocument).
 */
class MopeDocumentRepository extends ModelRepository
{
}
